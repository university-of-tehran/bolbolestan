package CommandInterface;

import Models.Schedule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.SCHEDULE_DOES_NOT_EXIST;
import static CommandInterface.Constants.SCHEDULE_FILE_PATH;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class RemoveScheduleTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(SCHEDULE_FILE_PATH);
            writer.print("[{\"StudentId\": \"810196123\", \"code\": \"81013602\"},{\"StudentId\": \"810196121\", \"code\": \"81013601\"}]");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testRemoveScheduleExists() {
        Parser.parse("removeFromWeeklySchedule {\"StudentId\": \"810196123\", \"code\": \"81013602\"}");
        List<Schedule> schedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(schedules.size(), 1);
    }
    
    @Test
    public void testRemoveScheduleDoesNotExists() {
        Parser.parse("removeFromWeeklySchedule {\"StudentId\": \"820196123\", \"code\": \"82013602\"}");
        List<Schedule> schedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(schedules.size(), 2);
        assertThat(outContent.toString(), containsString("{\"success\":false,\"error\":\"The schedule you\\u0027re looking for does not exist.\"}"));
    }
}