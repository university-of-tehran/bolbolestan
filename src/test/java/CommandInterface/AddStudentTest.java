package CommandInterface;

import Models.Offering;
import Models.Student;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.OFFERINGS_FILE_PATH;
import static CommandInterface.Constants.STUDENTS_FILE_PATH;
import static org.junit.Assert.assertEquals;

public class AddStudentTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(STUDENTS_FILE_PATH);
            writer.print("[]");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testAddStudent() {
        Parser.parse("addStudent {\"studentId\": \"810196123\", \"name\": \"Ali\", \"enteredAt\": \"1396\"}");
        List<Student> students = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(STUDENTS_FILE_PATH));
            Type studentsListType = new TypeToken<ArrayList<Student>>(){}.getType();
            students = gson.fromJson(reader, studentsListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(students.size(), 1);
    }
}