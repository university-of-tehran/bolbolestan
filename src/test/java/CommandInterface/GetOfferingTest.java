package CommandInterface;

import Models.Offering;
import Models.Schedule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class GetOfferingTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(OFFERINGS_FILE_PATH);
            writer.print("[{\"code\":\"81013602\",\"name\":\"Internet Engineering\",\"Instructor\":\"Ehsan Khamespanah\",\"units\":3,\"classTime\":{\"days\":[\"saturday\",\"Monday\"],\"time\":\"16-17:30\"},\"examTime\":{\"start\":\"2021-9-01T08:00:00\",\"end\":\"2021-9-01T08:00:00\"},\"capacity\":60,\"prerequisites\":[\"Advanced Programming\",\"Operating Systems\"]}]");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testGetOffering() {
        Parser.parse("getOffering {\"studentId\": \"810196123\", \"code\": \"81013602\"}");

        assertThat(outContent.toString(), containsString("{\"success\":true,\"data\":\"{\\\"code\\\":\\\"81013602\\\",\\\"name\\\":\\\"Internet Engineering\\\",\\\"Instructor\\\":\\\"Ehsan Khamespanah\\\",\\\"units\\\":3,\\\"classTime\\\":{\\\"days\\\":[\\\"saturday\\\",\\\"Monday\\\"],\\\"time\\\":\\\"16-17:30\\\"},\\\"examTime\\\":{\\\"start\\\":\\\"2021-9-01T08:00:00\\\",\\\"end\\\":\\\"2021-9-01T08:00:00\\\"},\\\"capacity\\\":60,\\\"prerequisites\\\":[\\\"Advanced Programming\\\",\\\"Operating Systems\\\"]}\"}"));
    }
}