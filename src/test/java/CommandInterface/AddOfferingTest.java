package CommandInterface;

import Models.Offering;
import Models.Schedule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class AddOfferingTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(OFFERINGS_FILE_PATH);
            writer.print("[]");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testAddOffering() {
        Parser.parse("addOffering {\"code\": \"81013602\", \"name\": \"Internet Engineering\", \"Instructor\": \"Ehsan Khamespanah\", \"units\": 3, \"classTime\": {\"days\": [\"saturday\", \"Monday\"], \"time\": \"16-17:30\"}, \"examTime\": {\"start\": \"2021-9-01T08:00:00\", \"end\": \"2021-9-01T08:00:00\"}, \"capacity\": 60, \"prerequisites\": [\"Advanced Programming\", \"Operating Systems\"]}");
        List<Offering> offerings = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(OFFERINGS_FILE_PATH));
            Type offeringsListType = new TypeToken<ArrayList<Offering>>(){}.getType();
            offerings = gson.fromJson(reader, offeringsListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(offerings.size(), 1);
    }
}