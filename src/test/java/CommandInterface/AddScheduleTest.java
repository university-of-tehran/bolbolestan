package CommandInterface;

import Models.Schedule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.OFFERINGS_FILE_PATH;
import static CommandInterface.Constants.SCHEDULE_FILE_PATH;
import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;


public class AddScheduleTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(OFFERINGS_FILE_PATH);
            writer.print("[]");
            writer.close();
            writer = new PrintWriter(SCHEDULE_FILE_PATH);
            writer.print("[]");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testOfferingExists() {
        Parser.parse("addOffering {\"code\": \"81013602\", \"name\": \"Internet Engineering\", \"Instructor\": \"Ehsan Khamespanah\", \"units\": 3, \"classTime\": {\"days\": [\"saturday\", \"Monday\"], \"time\": \"16-17:30\"}, \"examTime\": {\"start\": \"2021-9-01T08:00:00\", \"end\": \"2021-9-01T08:00:00\"}, \"capacity\": 60, \"prerequisites\": [\"Advanced Programming\", \"Operating Systems\"]}");
        Parser.parse("addToWeeklySchedule {\"StudentId\": \"810196123\", \"code\": \"81013602\"}");
        List<Schedule> schedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(schedules.size(), 1);
    }
    
    @Test
    public void testOfferingDoesNotExist() {
        Parser.parse("addOffering {\"code\": \"81013602\", \"name\": \"Internet Engineering\", \"Instructor\": \"Ehsan Khamespanah\", \"units\": 3, \"classTime\": {\"days\": [\"saturday\", \"Monday\"], \"time\": \"16-17:30\"}, \"examTime\": {\"start\": \"2021-9-01T08:00:00\", \"end\": \"2021-9-01T08:00:00\"}, \"capacity\": 60, \"prerequisites\": [\"Advanced Programming\", \"Operating Systems\"]}");
        Parser.parse("addToWeeklySchedule {\"StudentId\": \"810196123\", \"code\": \"81012434\"}");
        List<Schedule> schedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(schedules.size(), 0);
        assertThat(outContent.toString(), containsString(Constants.OFFER_DOES_NOT_EXIST));
    }
}