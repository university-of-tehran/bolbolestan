package CommandInterface;

public class Constants {
    public static final String OFFERINGS_FILE_PATH = "src/main/resources/offerings.json";
    public static final String STUDENTS_FILE_PATH = "src/main/resources/students.json";
    public static final String SCHEDULE_FILE_PATH = "src/main/resources/schedules.json";
    public static final String ADD_OFFERING = "addOffering";
    public static final String ADD_STUDENT = "addStudent";
    public static final String GET_OFFERINGS = "getOfferings";
    public static final String GET_OFFERING = "getOffering";
    public static final String ADD_SCHEDULE = "addToWeeklySchedule";
    public static final String REMOVE_SCHEDULE = "removeFromWeeklySchedule";
    public static final String GET_SCHEDULE = "getWeeklySchedule";
    public static final String FINALIZE = "finalize";
    public static final String OFFER_DOES_NOT_EXIST = "The offering with this code does not exist.";
    public static final String SCHEDULE_DOES_NOT_EXIST = "The schedule you're looking for does not exist.";
    public static final String STUDENT_SCHEDULE_DOES_NOT_EXIST = "The student you're looking for does not exist in schedules.";
    public static final int MIN_VALID_UNITS = 12;
    public static final int MAX_VALID_UNITS = 20;
    public static final String MINIMUM_UNITS_ERROR = "MinimumUnitsError";
    public static final String MAXIMUM_UNITS_ERROR = "MaximumUnitsError";
    public static final String CLASS_TIME_COLLISION_ERROR = "ClassTimeCollisionError";
    public static final String EXAM_TIME_COLLISION_ERROR = "ExamTimeCollisionError";
    public static final String CAPACITY_ERROR = "CapacityError";
}
