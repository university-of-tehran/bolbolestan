package CommandInterface;

import Handler.*;
import Models.ErrorResponse;
import Models.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Parser {
    public static void parse(String cmd) {
        String[] slicedCommand = cmd.split(" ",2);
        String command = slicedCommand[0];
        String data = "";
        Response response;
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        if (slicedCommand.length > 1) {
            data = slicedCommand[1];
        }

        switch(command) {
            case Constants.ADD_OFFERING:
                response = AddOfferingHandler.handle(data);
                break;
            case Constants.ADD_STUDENT:
                response = AddStudentHandler.handle(data);
                break;
            case Constants.GET_OFFERINGS:
                response = GetOfferingsHandler.handle(data);
                break;
            case Constants.GET_OFFERING:
                response = GetOfferingHandler.handle(data);
                break;
            case Constants.ADD_SCHEDULE:
                response = AddScheduleHandler.handle(data);
                break;
            case Constants.REMOVE_SCHEDULE:
                response = RemoveScheduleHandler.handle(data);
                break;
            case Constants.GET_SCHEDULE:
                response = GetScheduleHandler.handle(data);
                break;
            case Constants.FINALIZE:
                response = FinalizeHandler.handle(data);
                break;
            default:
                response = new ErrorResponse();
                break;
        }
        System.out.println(gson.toJson(response));
    }
}
