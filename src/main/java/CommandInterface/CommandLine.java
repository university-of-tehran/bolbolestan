package CommandInterface;
import java.util.Scanner;

public class CommandLine {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        while (!s.equals("exit")) {
            Parser.parse(s);
            s = in.nextLine();
        }
    }
}
