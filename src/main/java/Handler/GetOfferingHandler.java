package Handler;

import Models.*;
import Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;

import static CommandInterface.Constants.OFFER_DOES_NOT_EXIST;


public class GetOfferingHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        ErrorResponse errorResponse = new ErrorResponse();
        List<Offering> offerings = Utils.getOfferingsFromFile();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Schedule schedule = gson.fromJson(data, Schedule.class);

        for (Offering offering: offerings) {
            if (offering.code.equals(schedule.code)) {
                successResponse.data = gson.toJson(offering);
                return successResponse;
            }
        }
        errorResponse.error = OFFER_DOES_NOT_EXIST;
        return errorResponse;
    }
}