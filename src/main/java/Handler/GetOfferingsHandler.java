package Handler;

import Models.*;
import Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;

public class GetOfferingsHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        List<Offering> offerings = Utils.getOfferingsFromFile();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        List<PrintableOffering> printableOfferings = new ArrayList<>();
        for (Offering offering: offerings) {
            PrintableOffering printableOffering = new PrintableOffering();
            printableOffering.code = offering.code;
            printableOffering.name = offering.name;
            printableOffering.Instructor = offering.Instructor;

            printableOfferings.add(printableOffering);
        }

        String printableOfferingsInJson = gson.toJson(printableOfferings);
        successResponse.data = printableOfferingsInJson;
        return successResponse;
    }
}