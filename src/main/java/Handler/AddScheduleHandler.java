package Handler;

import Models.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.OFFERINGS_FILE_PATH;
import static CommandInterface.Constants.SCHEDULE_FILE_PATH;
import static CommandInterface.Constants.OFFER_DOES_NOT_EXIST;

public class AddScheduleHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        ErrorResponse errorResponse = new ErrorResponse();
        List<Schedule> schedules;
        List<Offering> offerings;
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        File schedulesFile = new File(SCHEDULE_FILE_PATH);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
            reader = new BufferedReader(new FileReader(OFFERINGS_FILE_PATH));
            Type offeringListType = new TypeToken<ArrayList<Offering>>(){}.getType();
            offerings = gson.fromJson(reader, offeringListType);
        } catch (IOException e) {
            e.printStackTrace();
            errorResponse.error = e.toString();
            return errorResponse;
        }

        Schedule schedule = gson.fromJson(data, Schedule.class);
        List<String> offerCodes = new ArrayList<>();
        for(Offering item : offerings) {
            offerCodes.add(item.code);
        }

        if(offerCodes.contains(schedule.code)) {
            schedules.add(schedule);
        } else {
            errorResponse.error = OFFER_DOES_NOT_EXIST;
            return errorResponse;
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(schedulesFile));
            gson.toJson(schedules, writer);
            writer.flush();
            writer.close();
            return successResponse;
        } catch (IOException e) {
            e.printStackTrace();
            errorResponse.error = e.toString();
            return errorResponse;
        }
    }
}
