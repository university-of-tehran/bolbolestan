package Handler;

import Models.ErrorResponse;
import Models.Response;
import Models.Student;
import Models.SuccessResponse;
import Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.STUDENTS_FILE_PATH;

public class AddStudentHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        ErrorResponse errorResponse = new ErrorResponse();
        List<Student> students = Utils.getStudentsFromFile();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Student student = gson.fromJson(data, Student.class);
        students.add(student);

        File studentsFile = new File(STUDENTS_FILE_PATH);

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(studentsFile));
            gson.toJson(students, writer);
            writer.flush();
            writer.close();
            return successResponse;
        } catch (IOException e) {
            e.printStackTrace();
            errorResponse.error = e.toString();
            return errorResponse;
        }
    }
}
