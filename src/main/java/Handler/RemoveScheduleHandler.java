package Handler;

import Models.ErrorResponse;
import Models.Response;
import Models.Schedule;
import Models.SuccessResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.*;

public class RemoveScheduleHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        ErrorResponse errorResponse = new ErrorResponse();
        List<Schedule> schedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        File schedulesFile = new File(SCHEDULE_FILE_PATH);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
            errorResponse.error = e.toString();
            return errorResponse;
        }

        Schedule schedule = gson.fromJson(data, Schedule.class);
        boolean flag = false;

        for(Schedule item : schedules) {
            if (item.code.equals(schedule.code) && item.StudentId.equals(schedule.StudentId)) {
                flag = true;
                schedules.remove(item);
            }
        }

        if(flag) {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(schedulesFile));
                gson.toJson(schedules, writer);
                writer.flush();
                writer.close();
                return successResponse;
            } catch (IOException e) {
                e.printStackTrace();
                errorResponse.error = e.toString();
                return errorResponse;
            }
        } else {
            errorResponse.error = SCHEDULE_DOES_NOT_EXIST;
            return errorResponse;
        }
    }
}
