package Handler;

import Models.ErrorResponse;
import Models.Offering;
import Models.Response;
import Models.SuccessResponse;
import Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.List;

import static CommandInterface.Constants.OFFERINGS_FILE_PATH;


public class AddOfferingHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        ErrorResponse errorResponse = new ErrorResponse();
        List<Offering> offerings = Utils.getOfferingsFromFile();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Offering offering = gson.fromJson(data, Offering.class);
        offerings.add(offering);

        File offeringsFile = new File(OFFERINGS_FILE_PATH);

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(offeringsFile));
            gson.toJson(offerings, writer);
            writer.flush();
            writer.close();
            return successResponse;
        } catch (IOException e) {
            e.printStackTrace();
            errorResponse.error = e.toString();
            return errorResponse;
        }
    }
}