package Handler;

import Models.*;
import Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.*;

public class FinalizeHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        ErrorResponse errorResponse = new ErrorResponse();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        FinalizeInput input = gson.fromJson(data, FinalizeInput.class);

        List<Schedule> schedules = Utils.getSchedulesFromFile();
        List<Schedule> student_schedules = new ArrayList<>();

        for (Schedule schedule: schedules) {
            if (schedule.StudentId.equals(input.StudentId)) {
                student_schedules.add(schedule);
            }
        }

        if (student_schedules.size() < 1) {
            errorResponse.error = STUDENT_SCHEDULE_DOES_NOT_EXIST;
            return errorResponse;
        }

        List<Offering> offerings = Utils.getOfferingsFromFile();
        List<Offering> student_offerings = new ArrayList<>();

        for (Schedule schedule: schedules) {
            for (Offering offering: offerings) {
                if (schedule.code.equals(offering.code)) {
                    student_offerings.add(offering);
                }
            }
        }

        String err1 = FinalizeHandler.CheckUnits(student_offerings);
        if(err1 != null && !err1.isEmpty()) {
            errorResponse.error = err1;
            return errorResponse;
        }
        String err2 = FinalizeHandler.CheckClassTimeCollision(student_offerings);
        if(err2 != null && !err2.isEmpty()) {
            errorResponse.error = err2;
            return errorResponse;
        }
        String err3 = FinalizeHandler.CheckExamTimeCollision(student_offerings);
        if(err3 != null && !err3.isEmpty()) {
            errorResponse.error = err3;
            return errorResponse;
        }
        String err4 = FinalizeHandler.CheckCapacity(student_offerings, schedules);
        if(err4 != null && !err4.isEmpty()) {
            errorResponse.error = err4;
            return errorResponse;
        }

        for (Schedule schedule: schedules) {
            if (schedule.StudentId.equals(input.StudentId)) {
                schedule.status = "finalized";
            }
        }

        File schedulesFile = new File(SCHEDULE_FILE_PATH);

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(schedulesFile));
            gson.toJson(schedules, writer);
            writer.flush();
            writer.close();
            return successResponse;
        } catch (IOException e) {
            e.printStackTrace();
            errorResponse.error = e.toString();
            return errorResponse;
        }
    }

    public static String CheckUnits(List<Offering> offerings) {
        int sum = 0;
        for (Offering offering: offerings) {
            sum += offering.units;
        }
        if (sum < MIN_VALID_UNITS) {
            return MINIMUM_UNITS_ERROR;
        }
        else if (sum > MAX_VALID_UNITS) {
            return MAXIMUM_UNITS_ERROR;
        }
        return null;
    }

    public static String CheckClassTimeCollision(List<Offering> offerings) {
        boolean day_collision = false;

        for (Offering o1: offerings) {
            for (Offering o2: offerings) {
                for (String d1: o1.classTime.days) {
                    for (String d2: o2.classTime.days) {
                        if (d1.equals(d2)) {
                            day_collision = true;
                            break;
                        }
                    }
                }
                if (day_collision) {
                    String[] t1 = (o1.classTime.time).split("-");
                    String[] t2 = (o2.classTime.time).split("-");
                    String s1 = t1[0];
                    String e1 = t1[1];
                    String s2 = t2[0];
                    String e2 = t2[1];
                    if ((s1.compareTo(s2) >= 0 && s1.compareTo(e2) < 0) ||(e1.compareTo(s2) > 0 && e1.compareTo(e2) <= 0)) {
                        return CLASS_TIME_COLLISION_ERROR + " <" + o1.code + "> <" + o2.code + ">";
                    }
                }
                day_collision = false;
            }
        }
        return null;
    }

    public static String CheckExamTimeCollision(List<Offering> offerings) {
        for (Offering o1: offerings) {
            for (Offering o2: offerings) {
                 String examTimeStart1 = o1.examTime.start;
                 String examTimeEnd1 = o1.examTime.end;
                 String examTimeStart2 = o2.examTime.start;
                 String examTimeEnd2 = o2.examTime.end;
                 if (
                     (examTimeStart1.compareTo(examTimeStart2) >= 0 && examTimeStart1.compareTo(examTimeEnd2) < 0) ||
                     (examTimeEnd1.compareTo(examTimeStart2) > 0 && examTimeEnd1.compareTo(examTimeEnd2) <= 0)
                 ) {
                     return EXAM_TIME_COLLISION_ERROR + " <" + o1.code + "> <" + o2.code + ">";
                 }
            }
        }
        return null;
    }

    public static String CheckCapacity(List<Offering> offerings, List<Schedule> schedules) {
        int sum;
        for (Offering offering: offerings) {
            sum = 0;
            for (Schedule schedule: schedules) {
                if (schedule.code.equals(offering.code) && schedule.status.equals("finalized")) {
                    sum += 1;
                }
            }
            if (sum > offering.capacity) {
                return CAPACITY_ERROR + " <" + offering.code + ">";
            }
        }
        return null;
    }
}
