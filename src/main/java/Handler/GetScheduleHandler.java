package Handler;

import Models.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.OFFERINGS_FILE_PATH;
import static CommandInterface.Constants.SCHEDULE_FILE_PATH;

public class GetScheduleHandler {
    public static Response handle(String data) {
        SuccessResponse successResponse = new SuccessResponse();
        ErrorResponse errorResponse = new ErrorResponse();
        List<Schedule> schedules = new ArrayList<>();
        List<Offering> offerings = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
            reader = new BufferedReader(new FileReader(OFFERINGS_FILE_PATH));
            Type offeringListType = new TypeToken<ArrayList<Offering>>(){}.getType();
            offerings = gson.fromJson(reader, offeringListType);
        } catch (IOException e) {
            e.printStackTrace();
            errorResponse.error = e.toString();
            return errorResponse;
        }

        String[] slicedData = data.split("\"");
        List<PrintableSchedule> printableSchedules = new ArrayList<>();

        for (Schedule item: schedules) {
            if (item.StudentId.equals(slicedData[3])) {
                for (Offering offer: offerings) {
                    if (offer.code.equals(item.code)) {
                        PrintableSchedule printableSchedule = new PrintableSchedule();
                        printableSchedule.code = offer.code;
                        printableSchedule.name = offer.name;
                        printableSchedule.classTime = offer.classTime;
                        printableSchedule.examTime = offer.examTime;
                        printableSchedule.status = item.status;
                        printableSchedules.add(printableSchedule);
                    }
                }
            }
        }

        successResponse.data = new Gson().toJson(printableSchedules);
        return successResponse;
    }
}
