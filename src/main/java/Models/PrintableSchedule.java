package Models;

public class PrintableSchedule {
    public String code;
    public String name;
    public String Instructor;
    public ClassTime classTime;
    public ExamTime examTime;
    public String status;
}
