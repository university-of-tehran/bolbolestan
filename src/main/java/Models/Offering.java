package Models;

public class Offering {
    public String code;
    public String name;
    public String Instructor;
    public int units;
    public ClassTime classTime;
    public ExamTime examTime;
    public int capacity;
    public String[] prerequisites;
}
