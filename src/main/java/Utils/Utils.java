package Utils;

import Models.Offering;
import Models.Student;
import Models.Schedule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static CommandInterface.Constants.STUDENTS_FILE_PATH;
import static CommandInterface.Constants.OFFERINGS_FILE_PATH;
import static CommandInterface.Constants.SCHEDULE_FILE_PATH;

public class Utils {
    public static List<Offering> getOfferingsFromFile() {
        List<Offering> offerings = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        File offeringsFile = new File(OFFERINGS_FILE_PATH);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(OFFERINGS_FILE_PATH));
            Type offeringListType = new TypeToken<ArrayList<Offering>>(){}.getType();
            offerings = gson.fromJson(reader, offeringListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return offerings;
    }

    public static List<Student> getStudentsFromFile() {
        List<Student> students = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        File studentsFile = new File(STUDENTS_FILE_PATH);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(STUDENTS_FILE_PATH));
            Type studentsListType = new TypeToken<ArrayList<Student>>(){}.getType();
            students = gson.fromJson(reader, studentsListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return students;
    }

    public static List<Schedule> getSchedulesFromFile() {
        List<Schedule> schedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        File schedulesFile = new File(SCHEDULE_FILE_PATH);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULE_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return schedules;
    }
}